package ld.a;
import java.io.*;
import android.app.*;
import android.widget.*;
import android.os.Bundle;
import java.util.zip.*;
import android.util.Log;


public class MiscUtils extends Activity
{

    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tv = (TextView)findViewById(R.id.textview);
        tv.setText("Output :" + "\n" + run("pm path com.candyrufusgames.survivalcraft2"));
    }

    public static String run(String command)
	{

        try
		{
            // Executes the command.
            Process process = Runtime.getRuntime().exec(command);
            // Reads stdout.
            // NOTE: You can write to stdin of the command using
            //       process.getOutputStream().
            BufferedReader reader = new BufferedReader(
				new InputStreamReader(process.getInputStream()));
            int read;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((read = reader.read(buffer)) > 0)
			{
                output.append(buffer, 0, read);
            }
            reader.close();

            // Waits for the command to finish.
            process.waitFor();

            return output.toString();
        }
		catch (IOException e)
		{
            throw new RuntimeException(e);
        }
		catch (InterruptedException e)
		{
            throw new RuntimeException(e);
        }
    }
	public static void exec(String command)
	{
		try
		{
			Runtime.getRuntime().exec(command);
		}
		catch (IOException e)
		{}
	}
	public static byte[] inputStreamToByteArray(InputStream is) throws IOException
	{
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		while ((nRead = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		buffer.flush();

		return buffer.toByteArray();
	}
	/**
	 * Unzip a zip file.  Will overwrite existing files.
	 * 
	 * @param zipFile Full path of the zip file you'd like to unzip.
	 * @param location Full path of the directory you'd like to unzip to (will be created if it doesn't exist).
	 */
	public static void unzip(String zipFile, String location, int BUFFER_SIZE) {
		int size;
		byte[] buffer = new byte[BUFFER_SIZE];

		try {
			if ( !location.endsWith(File.separator) ) {
				location += File.separator;
			}
			File f = new File(location);
			if(!f.isDirectory()) {
				f.mkdirs();
			}
			ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile), BUFFER_SIZE));
			try {
				ZipEntry ze = null;
				while ((ze = zin.getNextEntry()) != null) {
					String path = location + ze.getName();
					File unzipFile = new File(path);

					if (ze.isDirectory()) {
						if(!unzipFile.isDirectory()) {
							unzipFile.mkdirs();
						}
					} else {
						// check for and create parent directories if they don't exist
						File parentDir = unzipFile.getParentFile();
						if ( null != parentDir ) {
							if ( !parentDir.isDirectory() ) {
								parentDir.mkdirs();
							}
						}

						// unzip the file
						FileOutputStream out = new FileOutputStream(unzipFile, false);
						BufferedOutputStream fout = new BufferedOutputStream(out, BUFFER_SIZE);
						try {
							while ( (size = zin.read(buffer, 0, BUFFER_SIZE)) != -1 ) {
								fout.write(buffer, 0, size);
							}

							zin.closeEntry();
						}
						finally {
							fout.flush();
							fout.close();
						}
					}
				}
			}
			finally {
				zin.close();
			}
		}
		catch (Exception e) {
			Log.e("io.github.porygonzrocks.scmod", e.getMessage(), e);
		}
		}
	public static void unzip(String zipFile, String location)
		{
			unzip(zipFile, location, 16384); // Buffer size is random
		}
	public static void Void() {
		
	}
}
